#from requests import Session
from zeep import Client
from zeep import xsd

key ='ldSOsUGghWNcXKg7'
password = 'Tt6ovy6VQaGoA3OJfPQDtZ5Dn'
accountNumber='510087160'
meterNumber='118829080'
trackingNumber='403934084723025'

        #449044304137821 = Shipment information sent to FedEx
        #149331877648230 = Tendered
        #020207021381215 = Picked Up
        #403934084723025 = Arrived at FedEx location
        #920241085725456 = At local FedEx facility
        #568838414941 = At destination sort facility
        #039813852990618 = Departed FedEx location
        #231300687629630 = On FedEx vehicle for delivery
        #797806677146 = International shipment release
        #377101283611590 = Customer not available or business closed
        #852426136339213 = Local Delivery Restriction
        #797615467620 = Incorrect Address
        #957794015041323 = Unable to Deliver
        #076288115212522 = Returned to Sender / Shipper
        #581190049992 = International Clearance delay
        #122816215025810 = Delivered
        #843119172384577 = Hold at Location
        #070358180009382 = Shipment Canceled

client = Client(wsdl='wsdl/TrackService_v12.wsdl')

factory = client.type_factory('ns0')

userCredential = factory.WebAuthenticationCredential(Key=key,
                                                     Password=password)

localization = factory.Localization(LanguageCode='EN',
                                    LocaleCode='us')

packageIdentifier = factory.TrackPackageIdentifier(Type = 'TRACKING_NUMBER_OR_DOORTAG',
                                                   Value = trackingNumber)

webAuthentication = factory.WebAuthenticationDetail(ParentCredential=userCredential,
                                                    UserCredential=userCredential)

clientDetail = factory.ClientDetail(AccountNumber = accountNumber,
                                    MeterNumber = meterNumber,
                                    Localization=localization)

transactionDetail = factory.TransactionDetail(CustomerTransactionId='track by number',
                                              Localization =localization)

version = factory.VersionId(ServiceId='track',
                            Major='9',
                            Intermediate='1',
                            Minor='0')

selectionDetails = factory.TrackSelectionDetail(CarrierCode='FDXE',
                                                PackageIdentifier=packageIdentifier)

trackRequest = factory.TrackRequest(WebAuthenticationDetail=webAuthentication,
                                    ClientDetail=clientDetail,
                                    TransactionDetail=transactionDetail,
                                    Version=version,
                                    SelectionDetails=selectionDetails)

client.service.track(trackRequest)


