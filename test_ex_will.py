
import zeep
import urlparse
import urllib
import os
import requests
from zeep import Client
from zeep.wsse.username import UsernameToken
from requests import Session
from requests.auth import HTTPBasicAuth
from zeep.transports import Transport


class Command():
    def __init__(self, *args, **options):
        super(Command, self).__init__(*args, **options)

        # go get the wsdl and setup properties we'll need to populate for the soap request
        self.uri = 'wsdl/RateService_v18.wsdl'
        print('WSDL FILE: %s' % self.uri)
        self.RequestContext = None
        self.TrackPackageByReferenceSearchCriteria = None

        """@type: class"""
        self.client = None

    def load_wsdl(self):
        # set session to use basic authentication and use values from settings
        # note: be sure you have these settings in your local.py
        session = Session()
        #session.auth = HTTPBasicAuth(settings.PUROLATOR_USERNAME, settings.PUROLATOR_PASSWORD)
        transport = Transport(session=session)

        # set zeep client to use our wsdl and custom transport with basic auth
        self.client = zeep.Client(wsdl=self.uri, transport=transport)

        # do not trust that wsdl has correct endpoints and create them intentionally like this
        self.client.create_service(
            '{http://purolator.com/pws/service/v1}TrackingServiceEndpoint',
            'https://devwebservices.purolator.com/EWS/V1/Tracking/TrackingService.asmx')

    def load_types(self):
        # set our properties based on the types from wsdl
        self.RequestContext = self.client.get_type('{http://purolator.com/pws/datatypes/v1}RequestContext')
        self.TrackPackageByReferenceSearchCriteria = self.client.get_type(
            '{http://purolator.com/pws/datatypes/v1}TrackPackageByReferenceSearchCriteria')

    def request_trackpackagebyreferencesearchcriteria(self):
        tp = self.TrackPackageByReferenceSearchCriteria()
        tp.Reference = 'ref1'
        tp.DestinationPostalCode = 'V2S8B7'
        tp.DestinationCountryCode = ''
        tp.BillingAccountNumber = ''
        tp.ShipmentFromDate = ''
        tp.ShipmentToDate = '2009-02-06'
        return tp

    def request_context(self):
        context = self.RequestContext()
        context.Version = '1.2'
        context.Language = 'en'
        context.GroupID = ''
        context.RequestReference = 'jinx'
        context.UserToken = 'jinx'
        return context

    def test_basic_auth(self):
        # call methods to load our test object
        self.load_wsdl()
        self.load_types()

        # echo our custom object properties for visual double-check
        print('---------------------------------------------')
        print('------------ request context ----------------')
        print('---------------------------------------------')
        print self.request_context()
        print('---------------------------------------------')
        print('---- trackpackagebyrefenercesearchcriteria --')
        print('---------------------------------------------')
        print self.request_trackpackagebyreferencesearchcriteria()

        # call service using our populated objects and our custom header with basic auth
        self.client.service.TrackPackagesByReference(
            self.request_trackpackagebyreferencesearchcriteria(),
            _soapheaders={'RequestContext': self.request_context()}, )

    def handle(self, *args, **options):
        self.test_basic_auth()
        print('And Hello Jinx!')


